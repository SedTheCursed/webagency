$(function(){
	//Scrolling des liens.
    function scroll(cible){
        $('html, body').animate({
            scrollTop: $(cible).offset().top,
        }, 'slow');
    }

    $("a").on("click",function(e) {
        e.preventDefault();
        scroll($(e.target).attr("href"));
    });

    $("#layout button").on("click",function(){
        scroll("#services");
    });

	//Affichage du lien actif.
    $(window).on("scroll load", function(){
        var current="",
            affichage=$(document).scrollTop()+$(window).height()/2;

        $("section").each(function(){
            if(affichage+1>=$(this).offset().top){
                current="#"+this.id;
            }
        });

        $(".active").removeClass("active");
        $("a[href='"+current+"']").parent().addClass("active");
    });

    //Triage des projets
    $("#selectionProjet p").on("click",function(e){
        var cibles=$(e.target).attr("data-projet");

        $(".affiche").removeClass("affiche");
        $(e.target).addClass("affiche");
        $("#selectionProjet+div img:not([data-projet='"+cibles+"'])").hide();
        $("#selectionProjet+div img[data-projet='"+cibles+"']").show();

        console.log(cibles);
        console.log( $("#samples>div:not(."+cibles+")"));
        console.log($("#samples>div[class='"+cibles+"']"));

        if(cibles==="tous"){
            $("#samples>div").show();
        } else {
            $("#samples>div:not(."+cibles+")").hide();
            $("#samples>div[class='"+cibles+"']").show();
        }
    });

    //Affichage des messages des projets
    $("#samples>div").each(function(){
       var message="#"+this.id+">div";

       $(this).on({
           mouseover: function(e){
               $(message).show();
           },
           mouseout: function () {
               $(message).hide();
           }
        });
    });

    //Positionnement et hauteur de la carte et de l'overlay
    var contactDebut=$("#contact").offset().top,
        contactHeight=$("#contact").height(),
        paddingTop=$("#contact").css("padding-top"),
        marge=Number(paddingTop.slice(0,-2)),
        position=contactDebut+marge;
        taille=contactHeight;

    $("#map").css({
        "top": position,
        "height":taille
    });
    $("#overlay").css({
        "top": position,
        "height":taille
    });

	//Gestion du menu de l'entête, en version mobile.
	$("#entete button").on("click",function(){
		$("#entete nav").slideToggle();
	});
});

//Affichage de la carte
function pageCarte (){
    var mapElt=document.getElementById("map"),
        position={lat: 48.873163, lng: 2.350189},
        mapOptions={
            zoom:18,
            center: position,
        },
        map= new google.maps.Map(mapElt,mapOptions),
        marker= new google.maps.Marker({
            position: position,
            map: map,
            title: "WebAgency"
        });
}